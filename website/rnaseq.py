import os
import subprocess

class RNA_Seq_Pipeline():
    """Pipeline for Python RNA-Seq"""
    def __init__(self):
        # List of files (converted to fastq if not already) that will be analyzed
        self.sequence_files = []
        # Reference genome sequence file
        self.reference_genome = None
        # Do the following with command line tools:
        # 1. Perform Quality control for sequence_files using FastQC
        # 2. Perform sequence alignment using STAR
        # 3. Assign reads to genes using featurecounts
        # 4. Conduct size-factor normalization using edgeR from Bioconductor
        subprocess.call("fastq_analysis.sh")
        # Collect gene-ontology data (how?)
        # Display data using a variety of charts (heatmaps, PCA, tables, etc.)

