from django.db import models

class Project(models.Model):
    """Created by Users, Projects contain sequence data and analysis results"""
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)

class Sequence_File(models.Model):
    """Users can upload sequence files to projects"""
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    filepath = models.FileField()
    # Users can upload different file formats, but will always need to convert to fastq
    # Accepted file formats: *.fa, [...]
    filetype = models.CharField(max_length=30)
    # Sequence Alignment Map (SAM) file for this Sequence_File
    SAM = models.FileField(null=True, blank=True)
