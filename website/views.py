from django.shortcuts import render

def home(request):
    """Main Page"""
    return render(request, 'index.html')

def create_project(request):
    """Render project creation form"""
    return render(request, 'x.html')

def upload(request):
    """Render upload form (for sequence files etc.)"""
    return render()
